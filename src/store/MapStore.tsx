import superagent from 'superagent';
import create from 'zustand';
import BASE_URL from '../URL_CONFIG';
import { Coordinate, Model } from './Model';

interface MapStore {
  model: Model;
  coordinates: Coordinate[];
  loading: boolean;
  getCoordinates: () => void;
  getModel: (_id: string) => void;
}

export const useStore = create<MapStore>()((set, get) => ({
  model: { _id: '', characteristics: {}, type: '' },
  coordinates: [],
  loading: false,
  getCoordinates() {
    set({ loading: true });
    superagent.get(BASE_URL + 'get').end((error, response) => {
      if (error) throw new Error(`ERROR: ${error}`);

      const coordinates = response.body as Coordinate[];

      set({ coordinates, loading: false });
    });
  },
  getModel(_id) {
    set({ loading: true });
    superagent.get(BASE_URL + 'get/' + _id).end((error, response) => {
      if (error) throw new Error(`ERROR: ${error}`);

      const model = response.body as Model;

      set({ model, loading: false });
    });
  },
}));
