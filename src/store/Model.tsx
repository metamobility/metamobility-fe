export type Coordinate = {
  type: string;
  _id: string;
  characteristics: {
    latitude: string;
    longitude: string;
  };
};

export type Model = {
  type: string;
  _id: string;
  characteristics: Record<string, string>;
};
