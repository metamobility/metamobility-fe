import { Box, Flex, Image, Link } from '@chakra-ui/react';
import { Link as RouterLink } from 'react-router-dom';
import Logo from '../assets/Logo.png';

export const Header = () => {
  return (
    <Flex
      backgroundColor={'#11131A'}
      alignItems="center"
      justifyContent="space-between"
      shadow="base"
      p="6"
    >
      <Image src={Logo} alt="Zoobiblioarch Logo" h="auto" w="400px" />
      <Flex direction="column">
        <Flex gap={10} fontSize="inherit" fontWeight="bold" color="#228FCA">
          <Link as={RouterLink} to="/">
            Home
          </Link>
          <Link as={RouterLink} to="/map">
            Map
          </Link>
          <Link as={RouterLink} to="/team">
            Team
          </Link>
        </Flex>
        <Box alignSelf="end">
          <Link
            href="https://forms.gle/HHQeWzjwXwpQdNcV8"
            target="_blank"
            rel="noopener noreferrer"
            color="#228FCA"
            fontWeight="bold"
            style={{ marginTop: '-2cm' }} // Add custom margin-top in CSS
          >
            Submissions/Feedback
          </Link>
        </Box>
      </Flex>
    </Flex>
  );
};
