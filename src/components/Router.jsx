import { Box } from '@chakra-ui/react';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import { Map } from '../pages/Map';
import { Home } from '../pages/Home';
import { Team } from '../pages/Team';
import { Header } from './Header';

export const Router = () => (
  <Box backgroundColor="#11131A" width="100%" height="100%">
    <MemoryRouter>
      <Header />
      <Routes>
        <Route index element={<Home />} />
        <Route path="/map" element={<Map />} />
        <Route path="/team" element={<Team />} />
      </Routes>
    </MemoryRouter>
  </Box>
);
