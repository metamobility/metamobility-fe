// import './map.css';
import L, { LatLngExpression } from 'leaflet';
import { MapContainer, Marker, TileLayer } from 'react-leaflet';
import Logo from '../assets/map.svg';
import { Coordinate } from '../store/Model';

interface IMapComponentProps {
  origin: LatLngExpression;
  markers: Coordinate[];
  getModel: (_id: string) => void;
  onOpen: () => void;
}

const MapComponent = ({
  origin,
  markers,
  onOpen,
  getModel,
}: IMapComponentProps) => {
  const icons = [
    new L.Icon({
      iconUrl: Logo,
      iconRetinaUrl: Logo,
      iconSize: [32, 32],
      className: 'leaflet-div-icon-red',
    }),
    new L.Icon({
      iconUrl: Logo,
      iconRetinaUrl: Logo,
      iconSize: [32, 32],
      className: 'leaflet-div-icon-yellow',
    }),
    new L.Icon({
      iconUrl: Logo,
      iconRetinaUrl: Logo,
      iconSize: [32, 32],
      className: 'leaflet-div-icon-green',
    }),
    new L.Icon({
      iconUrl: Logo,
      iconRetinaUrl: Logo,
      iconSize: [32, 32],
      className: 'leaflet-div-icon',
    }),
  ];

  const getIcon = (accuracy?: string) => {
    switch (accuracy?.toLowerCase().split(' ')[0].trim()) {
      case 'human':
        return icons[0];
      case 'zooarchaeology':
        return icons[1];
      case 'archaeobotany':
        return icons[2];
      default:
        return icons[3];
    }
  };

  return (
    <>
      <MapContainer
        maxBounds={[
          [-90, -180],
          [90, 180],
        ]}
        center={origin}
        zoom={3}
        minZoom={3}
        maxBoundsViscosity={1.0}
      >
        <TileLayer
          attribution="Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012"
          url="https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}"
        />
        {markers.map(
          (marker: Coordinate) =>
            marker.characteristics.latitude &&
            marker.characteristics.longitude && (
              <Marker
                key={marker._id}
                icon={getIcon(marker.type)}
                position={{
                  lat: Number(marker.characteristics.latitude),
                  lng: Number(marker.characteristics.longitude),
                }}
                eventHandlers={{
                  click: (e: any) => {
                    getModel(marker._id);
                    onOpen();
                  },
                }}
              />
            )
        )}
      </MapContainer>
    </>
  );
};

export default MapComponent;
