import { ChakraProvider, theme } from '@chakra-ui/react';
import 'leaflet/dist/leaflet.css';
import * as React from 'react';
import * as ReactDOM from 'react-dom/client';
import { Router } from './components/Router';
import './index.css';
import * as serviceWorker from './serviceWorker';

const container = document.getElementById('root');
if (!container) throw new Error('Failed to find the root element');
const root = ReactDOM.createRoot(container);

root.render(
  <React.StrictMode>
    <ChakraProvider theme={theme}>
      <Router />
    </ChakraProvider>
  </React.StrictMode>
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorker.unregister();
