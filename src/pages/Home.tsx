import { Box, HStack, Image, Link, Text } from '@chakra-ui/react';
import AG_Leventis from '../assets/AGLeventis_Logo.jpg';
import MetaMobilityLogo from '../assets/MetaLogo.png';
import backgroundImg from '../assets/background.jpg';
import CyprusInstitute from '../assets/cyprus-institute.png';
import David_prize from '../assets/david_prize.png';
import RIF from '../assets/logos.png';

export const Home = () => {
  return (
    <Box
      maxW="100xl"
      backgroundImage={`url(${backgroundImg})`}
      backgroundSize="cover"
      backgroundPosition="center"
    >
      <Box backgroundColor="rgba(0,0,0,0.3)" width="100%" p={4}>
        <Text
          fontWeight="bold"
          fontSize="6xl"
          color="#228FCA"
          fontFamily="Raleway"
          mb="4"
        >
          Welcome to MetaBioarch!
        </Text>
        <Text
          fontSize="2xl"
          fontWeight="semibold"
          color="#228FCA"
          fontFamily="Raleway"
          mb="2"
        >
          Database for osteoarchaeological, zooarchaeological, and
          archaeobotanical studies in the Eastern Mediterranean and Middle East.
        </Text>
        <Text
          textAlign="justify"
          fontWeight="semibold"
          fontFamily="Raleway"
          color="#e9e9e9"
        >
          MetaBioarch is a database of published human osteoarchaeological,
          zooarchaeological and archaeobotanical data from Hellenistic and Roman
          contexts in the Central and Eastern Mediterranean, with some entries
          also from the Balkans and Middle East. For zooarchaeological and
          archaeobotanical data, MetaBioarch includes the frequency of different
          animal and plant species. For humans, it presents summary data on
          mechanical stress markers (degenerative joint disease, intervertebral
          disc disease, Schmorl's nodes, entheseal changes), physiological
          stress markers (cribra orbitalia, porotic hyperostosis, enamel
          hypoplasia, periostitis), paleomobility (cranial and dental nonmetric
          traits, cranial and dental metrics, strontium and oxygen isotopes), as
          well as dental diseases. The aim of this initiative is to promote
          larger-scale studies and meta-analyses towards a better understanding
          of mobility, economy, human-environment interactions and other key
          aspects of the Hellenistic and Roman societies. Users can search the
          database using the interactive map and various data filtering options.
          We would be grateful if you could contact us in order to bring to our
          attention corrections as well as to contribute additional data. The
          MetaBioarch database was created as part of the MetaMobility project
          (EXCELLENCE/0421/0376), which is implemented under the programme of
          social cohesion “THALIA 2021-2027” cofunded by the European Union,
          through Research and Innovation Foundation. This work has also been
          supported by the Dan David Prize.
        </Text>
        <HStack borderRadius={20} justifyContent="space-between" wrap="wrap">
          <Image
            src={MetaMobilityLogo}
            alt="MetaMobility logo"
            h="auto"
            w="240px"
          />
          <Image
            src={David_prize}
            alt="Dan David Prize logo"
            h="auto"
            w="230px"
          />
          <Image src={AG_Leventis} alt="AG Leventis logo" h="auto" w="100px" />
          <Link href="https://www.cyi.ac.cy/">
            <Image
              src={CyprusInstitute}
              w="115px"
              h="auto"
              alt="Cyprus Institute Logo"
            />
          </Link>
          <Image src={RIF} alt="RIF Logo" />
        </HStack>
      </Box>
    </Box>
  );
};
