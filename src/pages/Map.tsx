import {
  Box,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerHeader,
  DrawerOverlay,
  Skeleton,
  Stat,
  StatGroup,
  StatLabel,
  StatNumber,
  useDisclosure,
  useToast,
} from '@chakra-ui/react';
import { useEffect } from 'react';
import MapComponent from '../components/Map';
import { useStore } from '../store/MapStore';
import { capitalize } from '../utils/exporters';

export const Map = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const getCoordinates = useStore((state) => state.getCoordinates);
  const getModel = useStore((state) => state.getModel);
  const coordinates = useStore((state) => state.coordinates);
  const toast = useToast();

  useEffect(() => {
    if (getCoordinates) {
      getCoordinates();
    }
  }, [getCoordinates]);

  useEffect(() => {
    toast({
      title: 'Please wait',
      description: 'Loading...',
      status: 'info',
      duration: 1500,
      isClosable: true,
    });
  }, [toast]);

  return (
    <>
      <Box w="100%" h="70vh">
        <MapComponent
          origin={{ lat: 55, lng: 49 }}
          markers={coordinates}
          getModel={getModel}
          onOpen={onOpen}
        />
      </Box>
      <DrawerExample isOpen={isOpen} onClose={onClose} />
    </>
  );
};

function DrawerExample({
  isOpen,
  onClose,
}: {
  isOpen: boolean;
  onClose: () => void;
}) {
  const { type, characteristics } = useStore((state) => state.model);
  const loading = useStore((state) => state.loading);
  return (
    <>
      <Drawer size="xl" isOpen={isOpen} placement="bottom" onClose={onClose}>
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton />
          <DrawerHeader>
            <Skeleton isLoaded={!loading}>
              Type: {fixType(type.split(' ').map(capitalize).join(' '))}
            </Skeleton>
          </DrawerHeader>

          <DrawerBody>
            <StatGroup gap={10}>
              {Object.entries(characteristics).map(
                (entry) =>
                  entry[1] && (
                    <Skeleton isLoaded={!loading}>
                      <Stat>
                        <StatLabel>
                          {fixCharacticsName(
                            capitalize(entry[0].split('-').join(' '))
                          )}
                        </StatLabel>
                        <StatNumber>{fixValue(entry[1])}</StatNumber>
                      </Stat>
                    </Skeleton>
                  )
              )}
            </StatGroup>
          </DrawerBody>
          {/* Drawer footer removed all export options */}
          {/* <DrawerFooter>
            <Button
              disabled={loading}
              variant="outline"
              mr={3}
              onClick={() =>
                download(
                  `${type}-${characteristics.region}.csv`,
                  json2csv([{ type: type, ...characteristics }])
                )
              }
            >
              Export CSV
            </Button>
            <Button
              mr={3}
              disabled={loading}
              onClick={() =>
                download(
                  `${type}-${characteristics.region}.tsv`,
                  json2csv([{ type: type, ...characteristics }], {
                    delimiter: { field: '\t' },
                  })
                )
              }
              colorScheme="blue"
            >
              Export TSV
            </Button>
            <Button
              disabled={loading}
              onClick={() =>
                generatePDF(`${type}-${characteristics.region}.pdf`, {
                  type,
                  ...characteristics,
                })
              }
              colorScheme="green"
            >
              Export PDF
            </Button>
          </DrawerFooter> */}
        </DrawerContent>
      </Drawer>
    </>
  );
}

const fixValue = (value: string) => {
  if (value.toLowerCase() === 'x') {
    return 'Present';
  } else if (value.toLowerCase() === 'ind') {
    return 'Indeterminate';
  }
  return value;
};

const fixType = (type: string) => {
  if (type === 'Archaeobotany Archaeobotanical Material') {
    return 'Archaeobotanical Remains';
  }
  return type;
};

const fixCharacticsName = (key: string) => {
  const wrongKeys = [
    'Carex sp',
    'Triticum Cf Monococcum',
    'Triticum Aestivumdurum',
    'Pinus Halepensisbrutia',
    'Cf Maluspyrus Seed',
    'Pinus Nigrasylvestris',
    'Countryregion',
    'Goatsheep',
    'Sparidaecentracanthidae',
    'Molluscsshells',
    'Dogfox',
    'Dogjackal',
    '87sr86sr Enamel',
    '87sr86sr Dentine',
    'Didyma (heiligtum)',
  ];
  const CorrectKeys = [
    'Carex sp.',
    'Triticum cf. monococcum',
    'Triticum aestivum/durum',
    'Pinus halepensis/brutia',
    'cf. Malus/Pyrus seed',
    'Pinus nigra/sylvestris',
    'Country/region',
    'Goat/sheep',
    'Sparidae/Centracanthidae',
    'Molluscs/shells',
    'Dog/Fox',
    'Dog/Jackal',
    '87Sr/86Sr Enamel',
    '87Sr/86Sr Dentine',
    'Didyma (Heiligtum)',
  ];

  if (
    wrongKeys
      .map((k) => k.toLowerCase().trim())
      .includes(key.toLowerCase().trim())
  ) {
    const oldKeyIndex = wrongKeys
      .map((k) => k.toLowerCase().trim())
      .indexOf(key.toLowerCase().trim());
    return CorrectKeys[oldKeyIndex];
  } else if (key.toLowerCase().includes(' sp')) {
    return key.replace(/(?<!\w)sp(?!\w)/gi, 'sp.');
  } else if (key.toLowerCase().includes('cf')) {
    return key.replace(/(?<!\w)cf(?!\w)/gi, 'cf.');
  }
  return key;
};
