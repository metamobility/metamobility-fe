import { Avatar, Flex, Text } from '@chakra-ui/react';
import team5 from '../assets/evi.jpg';
import team6 from '../assets/panos.jpg';
import team1 from '../assets/team1.jpg';
import team2 from '../assets/team2.jpg';
import team3 from '../assets/team3.jpg';
import team4 from '../assets/team4.jpg';

export const Team = () => {
  return (
    <Flex direction="column" mx="200" my="20" gap={4} color="white">
      <Member name="Efthymia Nikita" src={team2}>
        Dr Efthymia Nikita is Associate Professor in Bioarchaeology at the
        Science and Technology in Archaeology and Culture Research Center
        (STARC) of The Cyprus Institute. She studies human skeletal remains
        across the Mediterranean from prehistoric to post-Medieval contexts,
        shedding light on health and disease, diet, activity, demography and
        other important aspects of life in the past. She earned a BA in
        Archaeology from the Aristotle University of Thessaloniki and a PhD in
        Biological Anthropology from the University of Cambridge. Prior to her
        appointment at STARC, she held post-doctoral posts at the British School
        at Athens and the American School of Classical Studies at Athens, as
        well as a Marie Skłodowska-Curie fellowship at the University of
        Sheffield. Her research has resulted in over 100 articles and book
        chapters, including the textbook ‘Osteoarchaeology: A Guide to the
        Macroscopic Study of Human Skeletal Remains’ (Elsevier 2017). She is
        currently the co-Editor of the Journal of Archaeological Science and she
        recently also co-Edited the second edition of the Encyclopedia of
        Archaeology (Elsevier). In 2022, she was awarded the Dan David prize for
        her contribution to the study of the past.
      </Member>
      <Member name="Mahmoud Mardini" src={team1}>
        Dr Mahmoud Mardini joined the Cyprus Institute as a PhD student in
        October 2019 after receiving his MSc degree in Osteoarchaeology at the
        University of Sheffield (United Kingdom, 2017) and his BA degree in
        Archaeology at the American University of Beirut (Lebanon, 2016). He
        completed his PhD in Science and Technology in Archaeology and Cultural
        Heritage in December 2023. As an early-career researcher, Mahmoud has
        extensive archaeological fieldwork experience, including over twelve
        excavations and four surveys in Lebanon. Shortly after his MSc and
        during his time at the University of Sheffield, Mahmoud gained
        laboratory and teaching experience by working as a laboratory assistant
        and demonstrator for a number of short courses and one MSc module. As an
        osteoarchaeologist, Mahmoud has worked with the Lebanese Department of
        Antiquities and the American University of Beirut on various human bone
        assemblages from Beirut, Byblos, Chekka, and Baalbek; temporally
        covering the Middle-Bronze Age to the Roman Period. In addition, as a
        zooarchaeologist, he has analysed two faunal assemblages from Beirut and
        Jabal Moussa. Mahmoud’s PhD project performed a biocultural study on
        life quality and mobility in different populations across Lebanon during
        the classical era of Roman ‘Phoenicia’. His work has been disseminated
        in dozens of publications.
      </Member>
      <Member name="Evi Margaritis" src={team5}>
        Dr Evi Margaritis is Associate Professor at the Science and Technology
        in Archaeology and Culture Research Center. She holds a BA from the
        University of Athens and an MSc in Environmental Archaeology and
        Palaeoeconomy from the University of Sheffield. She performed her PhD at
        the University of Cambridge. After post-doctoral grants at the Malcolm
        Wiener Laboratory for Archaeological Science and the Fitch Laboratory,
        she was awarded a Marie Curie Individual Fellowship at the University of
        Cambridge. Dr Margaritis has extensive experience working in Greece,
        Cyprus, Jordan and Italy and she is in charge of the archaeobotanical
        studies of research projects run by numerous universities and research
        institutions. She is one of the leading experts in archaeobotanical
        research in eastern Mediterranean and the only archaeobotanist based in
        Cyprus. Part of her research has focused on the emergence of complex
        societies through the study of archaeobotanical remains of olive and
        grape in the Aegean. She has attracted funding from the Gates Cambridge
        Trust, the Leventis Foundation, the Institute for Aegean Prehistory and
        the Mediterranean Archaeological Trust. Currently she is the assistant
        director of the Cambridge Keros Excavation Project, one of the most
        multidisciplinary projects in East Mediterranean, where she is in charge
        of the Environmental Studies of the project. Her publication record
        includes numerous papers in peer reviewed journals and chapters in
        collective volumes.
      </Member>

      <Member name="Panagiotis Koullouros" src={team6}>
        Panagiotis is a PhD candidate in Archaeobotany at the Science and
        Technology in Archaeology and Culture Research Center (STARC). He
        received his Bachelor’s degree in Environmental Science from the Queen
        Mary University of London (2017) and his Master’s degree in
        Environmental Archaeology from the University of Reading (2018). His
        research focuses on the study of charcoal remains from Greece and Cyprus
        during the 1st millennium BCE. He is interested in the reconstruction
        and evolution of past vegetation cover in the region of the Eastern
        Mediterranean during this period with emphasis on its impact on human
        societies and their practices.
      </Member>

      <Member name="Antonio Caruso" src={team3}>
        Antonio holds an MPhil in Quaternary, Prehistory and Archaeology
        (University of Ferrara) with a focus on Bioarchaeology, and a BA in
        Cultural Heritage (University of Catania). In 2016, he worked with the
        PAVE Research Group at the University of Cambridge, where he became
        familiar with cutting-edge technologies such as microCT/CT scanning. He
        subsequently used these technologies in his Master’s dissertation
        titled: A Jigsaw Puzzle from Mota Cave (Ethiopia): Three-Dimensional
        Virtual Reconstruction and Quantitative Analysis of a Very Fragmented
        Cranium. Antonio is currently enrolled as a PhD student at The Cyprus
        Institute under the supervision of Dr. Efthymia Nikita. As part of his
        PhD project, he is investigating Hellenistic human remains in order to
        gain insights on the lifestyle, ancestry and mobility patterns of people
        living in Sicily during the transition between the Greek colonisation
        and the Roman Empire. Since 2022, he has been working as a Research
        Assistant in different projects (H2020 Twinning project PROMISED and
        Cypriot Research and Innovation Foundation MetaMobility project),
        gaining important skills and experience.
      </Member>

      <Member name="Mohamad Mardini" src={team4}>
        <Text fontSize="smaller" textAlign="justify">
          Software Developer
        </Text>
      </Member>
    </Flex>
  );
};

interface MemberInfo {
  name: string;
  src: string;
  children: any;
}

const Member: React.FC<MemberInfo> = ({ name, src, children }) => {
  return (
    <Flex gap={4}>
      <Avatar size="2xl" name={name} src={src} />
      <Flex gap={4} direction="column">
        <Text color="#e9a542" fontSize="2xl">
          {name}
        </Text>
        <Text>{children}</Text>
      </Flex>
    </Flex>
  );
};
