let BASE_URL = 'https://metamobility-api.vercel.app/api/';

if (process.env.NODE_ENV === 'development') {
  BASE_URL = 'http://localhost:3001/api/';
}
export default BASE_URL;
