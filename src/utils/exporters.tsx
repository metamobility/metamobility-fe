export function download(filename: string, text: string) {
  var element = document.createElement('a');
  element.setAttribute(
    'href',
    'data:text/tab-separated-values;charset=utf-8,' + encodeURIComponent(text)
  );
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

export const generatePDF = (filename: string, model: any) => {
  import('./pdfUtil.js')
    .then((pdfMake) => {
      // @ts-ignore
      pdfMake = pdfMake.default;

      const content = Object.entries(model).map((entry) => {
        if (entry[1]) {
          let key = entry[0].split('-').map(capitalize).join(' ');
          return [
            { text: key },
            { text: entry[1], style: { fontSize: 18, bold: true } },
            ' ',
            ' ',
          ];
        }
        return undefined;
      });

      const docDefinition = {
        content,
      };
      // @ts-ignore
      pdfMake.createPdf(docDefinition).download(filename);
    })
    .catch((exception) => {
      console.error(exception);
    });
};
export function capitalize(str: string) {
  if (!str) {
    return '';
  }
  return str.charAt(0).toUpperCase() + str.slice(1);
}
