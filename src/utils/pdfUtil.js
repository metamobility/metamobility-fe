import pdfMake from 'pdfmake/build/pdfmake.min';
import pdfFont from 'pdfmake/build/vfs_fonts';

pdfMake.vfs = pdfFont.pdfMake.vfs;

export default pdfMake;
